//
// Created by sergey on 22.01.2021.
//
#include "mem_internals.h"
#include "mem.h"


void alloc_test(){


    _malloc(10);
    _malloc(100);
    _malloc(800);



}

void free_block_test(){
    _malloc(40);

    void* m = _malloc(69);

    _malloc(301);

    _malloc(2);

    _free(m);

}

void free_two_blocks_test(){
    _malloc(13);
    void* i = _malloc(200);
    _malloc(250);
    void* j = _malloc(444);
    _free(i);
    _free(j);

}

void region_extension_test(){
    _malloc(10000);
    _malloc(30000);
}

void region_extension_psyched_test(){
    _malloc(1000000000);
    _malloc(2000);
}
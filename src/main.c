//
// Created by sergey on 22.01.2021.
//
#include "mem.h"

#include "tests.h"

int main(){

    void* heap = heap_init(30000);

    debug_heap(stdout,heap);

    printf("One \n");
    alloc_test();
    debug_heap(stdout,heap);
    free_heap(heap);

    printf("Two \n");
    free_block_test();
    debug_heap(stdout,heap);
    free_heap(heap);

    printf("Three \n");
    free_two_blocks_test();
    debug_heap(stdout,heap);
    free_heap(heap);

    printf("Four \n");
    region_extension_test();
    debug_heap(stdout,heap);
    free_heap(heap);

    printf("Five \n");
    region_extension_psyched_test();
    debug_heap(stdout,heap);
    free_heap(heap);
}
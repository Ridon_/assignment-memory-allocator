//
// Created by sergey on 22.01.2021.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H





void alloc_test();
void free_block_test();
void free_two_blocks_test();
void region_extension_test();
void region_extension_psyched_test();



#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
